# STM32与ESP8266实现ONENET平台数据与图片上传

## 项目简介

本项目是一个课程设计，旨在实现厨房环境中火灾和煤气泄漏的实时检测与报警。通过STM32微控制器采集温度传感器DS18b20、烟雾传感器MQ-7以及CO传感器MQ-2的数据，同时利用OV7670摄像头采集图片数据。这些数据通过ESP8266模块使用MQTT协议发送到ONENET物联网平台。

在ONENET平台上，用户可以通过可视化界面实时查看传感器数据，并调节各个传感器的报警阈值。当检测到异常情况时，系统会自动发送报警信息至用户指定的邮箱，确保用户能够及时采取措施。

## 功能特点

- **实时数据采集**：STM32微控制器实时采集温度、烟雾和CO传感器的数据。
- **图片采集**：OV7670摄像头用于采集厨房环境的图片数据。
- **数据上传**：通过ESP8266模块使用MQTT协议将数据上传至ONENET平台。
- **可视化监控**：ONENET平台提供可视化界面，实时显示传感器数据和图片。
- **报警功能**：用户可以设置报警阈值，当传感器数据超过阈值时，系统自动发送报警邮件。

## 硬件需求

- STM32微控制器
- ESP8266模块
- DS18b20温度传感器
- MQ-7烟雾传感器
- MQ-2 CO传感器
- OV7670摄像头
- 其他必要的电路元件

## 软件需求

- Keil uVision（用于STM32开发）
- Arduino IDE（用于ESP8266开发）
- ONENET物联网平台账号

## 使用说明

1. **硬件连接**：按照电路图连接STM32、ESP8266、传感器和摄像头。
2. **软件配置**：
   - 在Keil uVision中编写STM32的代码，实现传感器数据采集和图片采集。
   - 在Arduino IDE中编写ESP8266的代码，实现MQTT协议的数据上传。
3. **ONENET平台配置**：
   - 注册并登录ONENET平台。
   - 创建一个新的设备，获取设备ID和API密钥。
   - 配置数据流模板，用于接收传感器数据和图片数据。
4. **运行与测试**：
   - 将STM32和ESP8266的代码分别烧录到对应的硬件中。
   - 启动系统，观察ONENET平台上的数据更新情况。
   - 测试报警功能，确保报警邮件能够正常发送。

## 注意事项

- 确保所有传感器和摄像头连接正确，避免数据采集错误。
- 在ONENET平台上配置数据流模板时，注意数据格式的匹配。
- 测试报警功能时，确保邮箱配置正确，避免报警信息无法送达。

## 贡献

欢迎开发者为本项目贡献代码或提出改进建议。您可以通过提交Issue或Pull Request来参与项目的开发。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。

---

通过本项目，您可以学习到如何使用STM32和ESP8266实现物联网数据上传，并了解如何在ONENET平台上进行数据可视化和报警处理。希望本项目能够为您的学习和开发提供帮助！